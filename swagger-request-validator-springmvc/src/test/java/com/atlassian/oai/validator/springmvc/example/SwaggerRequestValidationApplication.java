//CHECKSTYLE:OFF // HideUtilityClassConstructor: this is no utility class
package com.atlassian.oai.validator.springmvc.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerRequestValidationApplication {

    public static void main(final String[] args) {
        SpringApplication.run(SwaggerRequestValidationApplication.class, args);
    }
}
//CHECKSTYLE:ON
